#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <bitset>

using namespace std;

#define BUFSIZE 1024
#define MAX_SEND_BUF 1000
#define MAX_REC_BUF 10000
char buf[BUFSIZE];
char temp_buf[BUFSIZE-9];
char rec_buf[BUFSIZE];
int last_pack_check = -1, baseptr = 1, currptr = 1, rflag = 0, chunk_rec = 1000, all_rec = 0, Mob = 0, fl = 1;
int sockfd; /* socket file descriptor - an ID to uniquely identify a socket by the application program */
int portno; /* port to listen on */
int clientlen; /* byte size of client's address */
int senderlen;
struct sockaddr_in senderaddr; /* sender's addr */
struct sockaddr_in clientaddr; /* client addr */
float dp = 0.4;
std::mutex mu, mu2, mu3;

struct send_pack
{
    int size;
    char pk[BUFSIZE];
    int seq;
};

void check_ack(char buf[], int *abc)
{
    char cbuf[10];
    int testc, rwnd;
    sscanf(buf,"%s %04d%04d", cbuf, &testc, &rwnd);
    *abc = rwnd;
    printf("Acknowledgement for packet no. %d and rwnd %d received\n", testc, *abc);
}

int drop()
{
    //srand(time(NULL));
    double random = (double)rand()/(double)RAND_MAX;
    //printf("random %f\n", random);
    if(random<dp)
        return 1;
    else
        return 0;
}

int minim(int a, int b)
{
    if (a<=b)
        return a;
    else return b;
}

int maxcalc(int a, int b)
{
    if (a>=b)
        return a;
    else return b;
}

void error(char *msg) {
    perror(msg);
    exit(0);
}
void check_send(char buf[])
{
    int seq;
    sscanf(buf,"1%04d", &seq);
    printf("Packet no. %d sent\n", seq);
}


int rec_calc(char buf[])
{
    char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);

    return testc;
}
vector<send_pack> send_buf;
send_pack dynbuf[MAX_REC_BUF];  //dynamic buffer on receiver side

int win = 3, ssthres = 100, cwin = 3;

void send_buffer_handle(char *temp_buf, int read_returns, int count)
{
    bzero(buf, BUFSIZE);
    sprintf(buf, "1%04d%04d", count, read_returns);
    memcpy(buf + 9, temp_buf, read_returns);
    send_pack temp;
    temp.size = read_returns + 9;
    temp.seq = count;
    memcpy(temp.pk, buf, read_returns + 9);
    send_buf.push_back(temp);
}


void udp_send(int fd, char *s, int size, int flag, const sockaddr *sadd, socklen_t slen)
{
    int i;
    i = sendto(fd, s, size , flag, sadd, slen);
    if (i == -1)
        error("Couldn't write to socket");
}

void ClSend()
{
    char *sbuf;
    sbuf = (char *)malloc(BUFSIZE);
    while (1)
    {
        if(all_rec == 1)
            break;
        while(currptr < minim(baseptr + win, send_buf.size()))
        {

            printf("bp: %d cp: %d\n", baseptr, currptr);
            bzero(sbuf, BUFSIZE);
            if(currptr >= send_buf.size())
                break;
            memcpy(sbuf, send_buf[currptr].pk, send_buf[currptr].size);
            check_send(sbuf);
            udp_send(sockfd, sbuf, send_buf[currptr].size , 0, (const sockaddr *)&senderaddr, senderlen);
            currptr++;
        }
    }
}

int parse_pack(char *nbuf)
{
    //printf("aaya = %c\n", nbuf[0]);
    return nbuf[0] == '1' ? 1 : 0;
}



bool tridupcheck(int val, int *a1, int *a2)
{
    if(*a1 == *a2)
    {
        if(val == *a1)
            return 1;
        else
        {
            *a1 = val;
            return 0;
        }
    }
    else
    {
        *(val != *a1 ? a1 : a2) = val;
        return 0;
    }
}



void window_update(char *rec_buf, int flag, int *prev_rec, int *a1, int *a2)
{
    int rec, rwnd;
    check_ack(rec_buf, &rwnd);
    if(flag == 1)
    {
        rec = rec_calc(rec_buf);
        if(rec == last_pack_check) {
            all_rec = 1;
        }
        if(rec > *prev_rec)
        {
            if(tridupcheck(rec, a1, a2))
            {
                ssthres = maxcalc(1, ssthres/=2);
                cwin = ssthres;
            }
            else
            {
                cwin += rec - baseptr + (cwin >= ssthres ? BUFSIZE / cwin : 1);
            }

            baseptr = rec + 1;
            *prev_rec = rec;
            win = minim(cwin, rwnd);
            printf("window for next iter: %d rwnd = %d\n", win, rwnd);

        }
    }
    else
    {
        printf("Window size reduced\n");
        ssthres = maxcalc(1, ssthres/=2);
        cwin = 1;
        win = minim(cwin, rwnd);
        currptr = baseptr;
    }
}

condition_variable cv1, cv2;
mutex m1,m2;
int aflag = 0;

void Recv(int fd)
{
    try {
        int n, prev_rec = 0, a1 = -1, a2 = -1;
        struct timeval tv;
        fd_set readfds;
        while (1) {
            tv.tv_sec = 1;
            tv.tv_usec = 000000;
            FD_ZERO(&readfds);
            FD_SET(fd, &readfds);
            select(fd + 1, &readfds, NULL, NULL, &tv);
            if (FD_ISSET(fd, &readfds)) {
                bzero(rec_buf, BUFSIZE);
                n = recvfrom(fd, rec_buf, BUFSIZE, 0, (sockaddr *) &clientaddr, (socklen_t *) &clientlen);
                if (n < 0)
                    error("ERROR in recvfrom");
                if (!parse_pack(rec_buf)) {
                    window_update(rec_buf, 1, &prev_rec, &a1, &a2);
                } else {
                    unique_lock<mutex> lk2(m2);
                    rflag = 1;
                    aflag = 0;
                    lk2.unlock();
                    cv2.notify_one();
                    unique_lock<mutex> lk1(m1);
                    cv1.wait(lk1, [] { return aflag; });

                }
            } else if (fl == 0) {
                window_update(rec_buf, 0, &prev_rec, &a1, &a2);
            }

        }
    }
    catch (int x) {
        cout << "Done\n";
    }
}


int cum_ack(bitset<MAX_REC_BUF>tt, int s)
{
    while(tt[s]!=0)
        s++;
    return s-1;
}

char fName[100];
void inrecv(int fd)
{
    int n, fSize;
    bzero(rec_buf, BUFSIZE);
    n = recvfrom(fd, rec_buf, BUFSIZE, 0, (sockaddr *) &clientaddr, (socklen_t *) &clientlen);
    if (n < 0)
        error("ERROR in recvfrom");
    sscanf(rec_buf, "1Filename: %s File_size: %d Total Chunks: %d", fName, &fSize, &chunk_rec);
    printf("Filename: %s of filesize: %d and total chunks %d being received\n", fName, fSize, chunk_rec);

}

void RecSend()
{
    try {
        int seq_no, pre_seq = 0, chunk_size, n, pn = 1, fSize, rwnd = MAX_REC_BUF, max_seq = 0;
        char fName[100];
        bitset<MAX_REC_BUF> rset;
        rset[0] = 1;
        while (1) {

            unique_lock<mutex> lk2(m2);
            cv2.wait(lk2, [] { return rflag; });

            if (!drop()) {

                sscanf(rec_buf, "1%04d%04d", &seq_no, &chunk_size);
                printf("%d\n", seq_no);
                if (seq_no <= pre_seq) {
                    rwnd = MAX_REC_BUF - pre_seq;
                    bzero(buf, BUFSIZE);
                    sprintf(buf, "0ACK %04d%04d", seq_no, rwnd);
                    printf("Acknowledgement for packet no. %d sent with rwnd %d\n", seq_no, rwnd);
                    udp_send(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
                } else {
                    send_pack temp;
                    temp.size = chunk_size;
                    temp.seq = seq_no;
                    memcpy(temp.pk, rec_buf, chunk_size + 9);
                    dynbuf[seq_no] = temp;
                    printf("seq = %d\n", seq_no);
                    rset[seq_no] = 1;
                    pre_seq = cum_ack(rset, Mob);
                    Mob = pre_seq;  //maximum occupied block
                    printf("MOB: %d\n", Mob);
                    rwnd = MAX_REC_BUF - pre_seq;
                    bzero(buf, BUFSIZE);
                    sprintf(buf, "0ACK %04d%04d", pre_seq, rwnd);
                    printf("Acknowledgement for packet no. %d sent with rwnd %d\n", pre_seq, rwnd);
                    udp_send(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
                }

            } else {
                printf("Packet dropped\n");
            }
            rflag = 0;
            unique_lock<mutex> lk1(m1);
            aflag = 1;
            lk1.unlock();
            cv1.notify_one();
        }
    }
    catch (int x) {
        cout << "Done\n";
    }
}


void appRecv()
{
    int i=1, mob;
    char tfname[100];
    sprintf(tfname, "serv_%s", fName);
    FILE *filefd = fopen(tfname,"wb");
    while(1)
    {
        mob = Mob;
        while(i<=mob)
        {
            printf("Sequence no of packet written = %d\n", dynbuf[i].seq);
            if (fwrite(dynbuf[i].pk + 9, 1, dynbuf[i].size, filefd) == -1)
                error("Couldn't write to file");
            i++;
        }
        if(i > chunk_rec)
            break;
    }
    fclose(filefd);
}

int chunk_calc(int fsize)
{
    int chunk = 0;
    while(fsize>0)
    {
        chunk++;
        fsize-=(BUFSIZE-9);
    }
    return chunk;
}

void insend(char *arg)
{
    FILE *filefd = fopen(arg, "rb");
    int file_size, chunk;
    fseek(filefd, 0, SEEK_END);
    file_size = ftell(filefd);
    rewind(filefd);

    chunk = chunk_calc(file_size);
    sprintf(buf,"1Filename: %s File_size: %d Total Chunks: %d", arg, file_size, chunk);
    printf("Filename: %s of filesize: %d and number of chunks %d being sent...\n", arg, file_size, chunk);

    send_pack tempo;
    memcpy(tempo.pk, buf, strlen(buf));
    send_buf.push_back(tempo);
    int n = sendto(sockfd, buf, strlen(buf), 0, (const sockaddr *)&senderaddr, senderlen);
    if (n < 0)
        error("ERROR writing to socket 1");
    printf("Yo\n");
}

void appSend(char *arg)
{
    FILE *filefd = fopen(arg, "rb");
    int read_returns, count = 0;

    while(1)
    {
        while(send_buf.size() - baseptr <= MAX_SEND_BUF)
        {
            bzero(temp_buf, BUFSIZE-9);
            read_returns = fread(temp_buf, 1, BUFSIZE-9, filefd);
            if (read_returns == -1)
                error("Couldn't read from file");
            if (read_returns == 0)
            {
                printf("File read over\n");
                last_pack_check = count;
                fclose(filefd);
                break;
            }
            count++;
            send_buffer_handle(temp_buf, read_returns, count);
        }
        if(last_pack_check == count)
            break;
    }
}



int main(int argc, char **argv)
{
    try {
        struct hostent *sender; /* client host info */
        char *hostname;
        int optval; /* flag value for setsockopt */
        char fName[BUFSIZE];
        int fSize, chunk, seq_no, pre_seq, read_return, chunk_size, portno2;

        if (atoi(argv[1]) == 0) {
            if (argc != 5) {
                fprintf(stderr,
                        "usage: %s <flag = 0/1 > 0: <hostname> <filename> <port_for_sender> && 1: <port_for_self> \n",
                        argv[0]);
                exit(1);
            }

            fl = 0;
            hostname = argv[2];
            portno = atoi(argv[4]);
        } else {
            if (argc != 3) {
                fprintf(stderr,
                        "usage: %s <flag = 0/1 > 0: <hostname> <filename> <port_for_sender> && 1: <port_for_self> \n",
                        argv[0]);
                exit(1);
            }
            portno2 = atoi(argv[2]);
        }

        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (sockfd < 0)
            error("ERROR opening socket");

        optval = 1;
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void *) &optval, sizeof(int));

        senderlen = sizeof(senderaddr);
        clientlen = sizeof(clientaddr);

        if (fl == 0) {
            sender = gethostbyname(hostname);
            if (sender == NULL) {
                fprintf(stderr, "ERROR, no such host as %s\n", hostname);
                exit(0);
            }
            bzero((char *) &senderaddr, sizeof(senderaddr));
            senderaddr.sin_family = AF_INET;
            bcopy(sender->h_addr, (char *) &senderaddr.sin_addr.s_addr, sender->h_length);
            senderaddr.sin_port = htons((unsigned short) portno);

            thread initialsend(insend, argv[3]);
            initialsend.join();
            thread appS(appSend, argv[3]);
            thread cls(ClSend);
            thread rec(Recv, sockfd);
            appS.join();
            cls.join();
        } else {
            struct sockaddr_in myaddr;
            bzero((char *) &myaddr, sizeof(myaddr));
            myaddr.sin_family = AF_INET;
            myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
            myaddr.sin_port = htons((unsigned short) portno2);

            if (bind(sockfd, (struct sockaddr *) &myaddr,
                     sizeof(myaddr)) < 0)
                error("ERROR on binding");

            thread initialrecv(inrecv, sockfd);
            initialrecv.join();
            thread appR(appRecv);
            thread recs(RecSend);
            thread rec(Recv, sockfd);

            appR.join();
        }
    }
    catch (int x) {
        cout << "Done\n";
    }
}
