#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <bitset>

using namespace std;

#define BUFSIZE 1024
#define MAX_SEND_BUF 1000
#define MAX_REC_BUF 10000
char buf[BUFSIZE];
char temp_buf[BUFSIZE-9];
char rec_buf[BUFSIZE];
int last_pack_check = -1, baseptr = 1, currptr = 1, rflag = 0, chunk = 0, all_rec = 0, Mob;
int sockfd; /* socket file descriptor - an ID to uniquely identify a socket by the application program */
int portno; /* port to listen on */
int clientlen; /* byte size of client's address */
int senderlen;
struct sockaddr_in senderaddr; /* sender's addr */
struct sockaddr_in clientaddr; /* client addr */
float dp = 0.01;
std::mutex mu, mu2;

struct send_pack
{
    int size;
    char pk[BUFSIZE];
    int seq;
};

void check_ack(char buf[])
{
    char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);
    printf("Acknowledgement for packet no. %d received\n", testc);
}

int drop()
{
    //srand(time(NULL));
    double random = (double)rand()/(double)RAND_MAX;
    //printf("random %f\n", random);
    if(random<=dp)
        return 1;
    else
        return 0;
}

int min(int a, int b)
{
    if (a<=b)
        return a;
    else return b;
}

void error(char *msg) {
    perror(msg);
    exit(0);
}
void check_send(char buf[])
{
    int seq;
    sscanf(buf,"%04d", &seq);
    printf("Packet no. %d sent\n", seq);
}


int rec_calc(char buf[])
{
    char cbuf[10];
    int testc;
    sscanf(buf,"%s %04d", cbuf, &testc);

    return testc;
}
vector<send_pack> send_buf;
send_pack dynbuf[MAX_REC_BUF];  //dynamic buffer on receiver side

int win = 3, ssthres = 100;

void send_buffer_handle(char *temp_buf, int read_returns, int count)
{
    bzero(buf, BUFSIZE);
    sprintf(buf, "1%04d%04d", count, read_returns);
    memcpy(buf + 9, temp_buf, read_returns);
    send_pack temp;
    temp.size = read_returns + 9;
    temp.seq = count;
    memcpy(temp.pk, buf, read_returns + 9);
    send_buf.push_back(temp);
}


void udp_send(int fd, char *s, int size, int flag, const sockaddr *sadd, socklen_t slen)
{
    int i;
    i = sendto(fd, s, size , flag, sadd, slen);
    if (i == -1)
        error("Couldn't write to socket");
}

void ClSend()
{
    char *sbuf;
    sbuf = (char *)malloc(BUFSIZE);
    while (1)
    {
        while(currptr < min(baseptr + win, send_buf.size()))
        {

            printf("bp: %d cp: %d\n", baseptr, currptr);
            bzero(sbuf, BUFSIZE);
            if(currptr >= send_buf.size())
                break;
            memcpy(sbuf, send_buf[currptr].pk, send_buf[currptr].size);
            check_send(sbuf);
            udp_send(sockfd, sbuf, send_buf[currptr].size , 0, (const sockaddr *)&senderaddr, senderlen);
            currptr++;
        }
    }
}

int parse_pack(char *nbuf)
{
    char c;
    sscanf(nbuf, "%c", c);
    return c == '1' ? 1 : 0;
}



bool tridupcheck(int val, int *a1, int *a2)
{
    if(*a1 == *a2)
    {
        if(val == *a1)
            return 1;
        else
        {
            *a1 = val;
            return 0;
        }
    }
    else
    {
        *(val != *a1 ? a1 : a2) = val;
        return 0;
    }
}



void window_update(char *rec_buf, int flag, int *prev_rec, int *a1, int *a2)
{
    mu2.lock();
    int rec;
    if(flag == 1)
    {
        rec = rec_calc(rec_buf);
        check_ack(rec_buf);
        if(rec == last_pack_check) {
            all_rec = 1;
        }
        if(rec > *prev_rec)
        {
            if(tridupcheck(rec, a1, a2))
            {
                ssthres = max(1, ssthres/=2);
                win = ssthres;
            }
            else
            {
                win += rec - baseptr + (win >= ssthres ? BUFSIZE / win : 1);
            }

            baseptr = rec + 1;
            *prev_rec = rec;
            printf("window for next iter: %d\n", win);

        }
    }
    else
    {
        printf("Window size reduced\n");
        ssthres = max(1, ssthres/=2);
        win = 1;
        currptr = baseptr;
    }
    mu2.unlock();
}


void Recv()
{
    int n, prev_rec = 0, a1 = -1, a2 = -1;
    struct timeval tv;
    fd_set readfds;
    while(1)
    {
        tv.tv_sec = 1;
        tv.tv_usec = 000000;
        FD_ZERO(&readfds);
        FD_SET(sockfd, &readfds);
        select(sockfd + 1, &readfds, NULL, NULL, &tv);
        if (FD_ISSET(sockfd, &readfds))
        {
            bzero(rec_buf, BUFSIZE);
            n = recvfrom(sockfd, rec_buf, BUFSIZE, 0, (sockaddr *) &clientaddr, (socklen_t *) &clientlen);
            if (n < 0)
                error("ERROR in recvfrom");
            if(!parse_pack(rec_buf))
                window_update(rec_buf, 1, &prev_rec, &a1, &a2);
            else
            {
                mu.lock();
                rflag = 1;
                mu.unlock();
            }
        }
        else
        {
            window_update(rec_buf, 0, &prev_rec, &a1, &a2);
        }

    }
}


int cum_ack(bitset<MAX_REC_BUF>tt, int s)
{
    while(tt[s]!=0)
        s++;
    return s-1;
}

void RecSend()
{
    int seq_no, pre_seq = 0, chunk_size, n, pn = 1, fSize;
    char fName[100];
    bitset<MAX_REC_BUF> rset;
    while(1)
    {
        if(pn == 1)
        {
            sscanf(buf, "1Filename: %s File_size: %d Total Chunks: %d", fName, &fSize, &chunk);
            printf("Filename: %s of filesize: %d being received\n", fName, fSize);
            pn = 0;
        }
        while(rflag != 1)
            ;


        if (!drop())
        {
            mu.lock();
            sscanf(rec_buf, "1%04d%04d", &seq_no, &chunk_size);
            if (seq_no <= pre_seq)
            {
                bzero(buf, BUFSIZE);
                sprintf(buf, "0ACK %04d", seq_no);
                printf("Acknowledgement for packet no. %d sent\n", seq_no);
                udp_send(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
            }
            else
            {
                send_pack temp;
                temp.size = chunk_size;
                temp.seq = seq_no;
                memcpy(temp.pk, buf, chunk_size + 9);
                dynbuf[seq_no] = temp;
                rset[seq_no] = 1;
                pre_seq = cum_ack(rset, seq_no);
                Mob = pre_seq;  //maximum occupied block

                bzero(buf, BUFSIZE);
                sprintf(buf, "0ACK %04d", pre_seq);
                printf("Acknowledgement for packet no. %d sent\n", pre_seq);
                udp_send(sockfd, buf, strlen(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
            }
            rflag = 0;
            mu.unlock();
        }
        else
        {
            printf("Packet dropped\n");
        }
    }
}


void appRecv()
{
    int i=1, mob;
    FILE *filefd = fopen("rec_file","wb");
    while(1)
    {
        mob = Mob;
        while(i<=mob)
        {
            if (fwrite(dynbuf[i].pk + 9, 1, dynbuf[i].size, filefd) == -1)
                error("Couldn't write to file");
            i++;
        }
        if(i > chunk)
            break;
    }
    fclose(filefd);
}

int chunk_calc(int fsize)
{
    int chunk = 0;
    while(fsize>0)
    {
        chunk++;
        fsize-=(BUFSIZE-9);
    }
}

void appSend(char **arg)
{
    FILE *filefd = fopen(arg[3], "rb");
    int read_returns, count = 0, file_size, chunk;
    fseek(filefd, 0, SEEK_END);
    file_size = ftell(filefd);
    rewind(filefd);

    chunk = chunk_calc(file_size);
    sprintf(buf,"1Filename: %s File_size: %d Total Chunks: %d", arg[3], file_size, chunk);
    printf("Filename: %s of filesize: %d being sent...\n", arg[3], file_size);

    send_pack tempo;
    memcpy(tempo.pk, buf, strlen(buf));
    send_buf.push_back(tempo);
    udp_send(sockfd, buf, BUFSIZE, 0, (const sockaddr *)&senderaddr, senderlen);

    while(1)
    {
        while(send_buf.size() - baseptr <= MAX_SEND_BUF)
        {
            bzero(temp_buf, BUFSIZE-9);
            read_returns = fread(temp_buf, 1, BUFSIZE-9, filefd);
            if (read_returns == -1)
                error("Couldn't read from file");
            if (read_returns == 0)
            {
                printf("File read over\n");
                last_pack_check = count;
                fclose(filefd);
                break;
            }
            count++;
            send_buffer_handle(temp_buf, read_returns, count);
        }
        if(last_pack_check == count)
            break;
    }
}



int main(int argc, char **argv)
{
    struct hostent *sender; /* client host info */
    char *hostname;
    int optval; /* flag value for setsockopt */
    char fName[BUFSIZE];
    int fSize, chunk, seq_no,pre_seq, read_return, chunk_size;

    if (argc != 4) {
        fprintf(stderr, "usage: %s <hostname> <port_for_sender> <filename> \n", argv[0]);
        exit(1);
    }
    hostname = argv[1];
    portno = atoi(argv[2]);

    //printf("Provide a value for drop probability\n");
    //scanf("%f", &dp);

    //printf("sender Running at port %d.....\n", portno);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");

    optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

    sender = gethostbyname(hostname);
    if (sender == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }
    
    bzero((char *) &senderaddr, sizeof(senderaddr));
    senderaddr.sin_family = AF_INET;
    bcopy(sender->h_addr, (char *)&senderaddr.sin_addr.s_addr, sender->h_length);
    senderaddr.sin_port = htons((unsigned short)portno);

    if (bind(sockfd, (struct sockaddr *) &senderaddr,
             sizeof(senderaddr)) < 0)
        error("ERROR on binding");

    clientlen = sizeof(clientaddr);

    std::thread appS(appSend, argv);
    std::thread appR(appRecv);
    std::thread cls(ClSend);
    std::thread rec(Recv);
    std::thread recs(RecSend);

    appS.join();
    appR.join();

}